package com.df.persistencia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Personas extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddNewPerson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personas);

        btnAddNewPerson = findViewById(R.id.addNewPerson);
        btnAddNewPerson.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        startActivity(new Intent(Personas.this, CrearPersona.class));
    }
}