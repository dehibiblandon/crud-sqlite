package com.df.persistencia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Carros extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddNewCar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carros);

        btnAddNewCar = findViewById(R.id.addNewCar);
        btnAddNewCar.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        startActivity(new Intent(Carros.this, CrearCarro.class));
    }
}